from django import forms
from lab_2.models import Note
from django.forms import ModelForm, widgets

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'dari', 'title', 'message']

        widgets = {
            'to' : forms.TextInput(attrs={'class':'form-control', 'placeholder':'Who is this note for?'}),
            'dari' : forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter your name!'}),
            'title' : forms.TextInput(attrs={'class':'form-control', 'placeholder':'What is this note about?'}),
            'message' : forms.Textarea(attrs={'class':'form-control', 'placeholder':'Enter your message here!'})
        }
        