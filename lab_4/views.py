from django.http import response
from django.shortcuts import render, HttpResponseRedirect
from lab_2.models import Note

from lab_4.forms import NoteForm

# Create your views here.

# method index untuk render semua instance Note
def index(request):
    all_notes = Note.objects.all()
    response = {'all_notes' : all_notes}
    return render(request, 'lab4_index.html', response)

# method untuk render form
def add_note(request):
    # Buat object form
    note_form = NoteForm(request.POST or None)

    #cek apakah form yang diisi valid
    #Jika valid, save lalu redirect ke path '/lab-4' untuk lihat bahwa Friend sudah bertambah
    if note_form.is_valid():
        note_form.save()
        return HttpResponseRedirect('/lab-4')
    
    response = {'note_form' : note_form}

    return render (request, 'lab4_form.html', response)

# method untuk render semua instance Note (digunakan untuk menampilkan Note dalam bentuk card)
def note_list(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)

