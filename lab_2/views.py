from django.shortcuts import render
from .models import Note

# import HttpRespnse dan serializers buat return XML format
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.

def index(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request, 'index_lab2.html', response)

# xml method untuk render XML
def xml(request):
    notes = Note.objects.all()
    data = serializers.serialize('xml', notes)
    return HttpResponse(data, content_type="application/xml")

# json method untuk render json
def json(request):
    notes = Note.objects.all()
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")