1. Perbedaan antara JSON dan XML
    JSON dan XML adalah jenis file yang digunakan untuk menyimpan dan
    melakukan transfer data melalui internet.
    JSON dan XML memiliki beberapa perbedaan:
    - Dari sisi penulisan
      Data pada XML dituliskan dengan menggunakan tags, mirip seperti pada HTML.
      Tags pada XML menunjukkan mengenai apa data yang ada di dalamnya.
      Data pada JSON direpresentasikan dengan dictionary. Keys menunjukkan mengenai apa
      data tersebut dan values menunjukkan nilai dari data tersebut
    
    - Tipe data yang didukung
      XML mendukung berbagai tipe data, seperti text, number, chart, image, dll.
      JSON hanya mendukung tipe data text dan number
    
    - Kecepatan parsing
      JSON memiliki size file yang lebih kecil dibandingkan XML. Maka dari itu, transfer dan
      pengambilan data menggunakan JSON berjalan lebih cepat dari XML

2. Perbedaan antara XML dan HTML
   XML dan HTML memang mirip karena suatu elemen diapit oleh tags dan memiliki atribut. Namun, 
   fungsi dari XML dan HTML berbeda. XML berfungsi sebagai sarana menampung dan transfer data.
   Sementara HTML berfungsi untuk mengatur tampilan dari sebuah website. Tags pada XML merupakan
   penunjuk mengenai apa data yang direpresentasikan oleh elemen. Sementara tags pada HTML merupakan
   penunjuk cara menampilkan elemen pada website. Maka dari itu, tags pada XML bisa disesuaikan oleh
   pembuat XML karena harus mendeskripsikan data, sementara tags pada HTML sudah ditentukan.