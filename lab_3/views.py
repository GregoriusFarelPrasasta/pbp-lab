from django.http import response
from django.shortcuts import render, HttpResponseRedirect

from lab_3.forms import FriendForm
from lab_1.models import Friend

from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends' : friends}
    return render(request, 'index_lab3.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):

    # Buat object form
    friend_form = FriendForm(request.POST or None)

    #cek apakah form yang diisi valid
    #Jika valid, save lalu redirect ke path '/lab-3' untuk lihat bahwa Friend sudah bertambah
    if friend_form.is_valid():
        friend_form.save()
        return HttpResponseRedirect('/lab-3')
    
    response = {'friend_form' : friend_form}

    return render (request, 'lab3_form.html', response)

