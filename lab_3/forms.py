from django import forms
from lab_1.models import Friend
from django.forms import ModelForm, widgets
from django import forms

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'tanggal_lahir']
        widgets = {
            'tanggal_lahir' : forms.DateInput(attrs={'type' : 'date'})           
            }




